<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pagesuite_en');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h^v{TB52;-EM&??#36>0ExJO]Fv>^f/%IU`l3#MZh5k/3k=a^Jl0Ttj 6v0N4R4}');
define('SECURE_AUTH_KEY',  'x>okQ4OR58vXz[4OBOi:/RW2C^5{d:cwLP>v5(YcDC;B@G?Bq0b=kBv^+NH!i@,5');
define('LOGGED_IN_KEY',    'J8wt6]R?9&9h5c!9DoOz)>`n+e;D68?`we`FNmq6)3Z?R(ok&>TVhI1=[4<j-9mU');
define('NONCE_KEY',        'oZB|/^kq!]z3]<HQvV$y#I KN+>xM.bXqVc;? g#D$8n?64/pBO83ZJm%[McQfj1');
define('AUTH_SALT',        '>ww|fH[4&d^%8iTaom.GGR0rD4!2I[D!zS[vE6Cl,,0*gtY Qs<tqdpQYN>mjp`m');
define('SECURE_AUTH_SALT', '2nt^{~=,Z63+CsjvZ]!WRbc/!_F5Xpt@Qr.|rVr68dfw.xWafY3qNC75Lxx76qDz');
define('LOGGED_IN_SALT',   '9A~nDH.H+fy/@C6.DVn 5|=TKmI5u?sp+:g/+]E4xJK7J=FLwG^YLk,7P.Y-d[t&');
define('NONCE_SALT',       ']5v;si.`mC$%1u0S:yT(~~xEj9S!gRXeRk!5p>3,^>2(K.q>:v6n)|kbx)OP>}O*');

/**#@-*/

// disable revisions so that the DB will not be overloaded with unnecessary content
define( 'WP_POST_REVISIONS', FALSE );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
