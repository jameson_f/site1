<?php
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    header("Location: https://$_SERVER[HTTP_HOST]/backend/wp-admin");
} else {
    header("Location: http://$_SERVER[HTTP_HOST]/backend/wp-admin");
}