<?php
date_default_timezone_set('Europe/London');

// REGISTER POST TYPES ####################################################################
add_action('init', 'customPosts');
function customPosts() {
  // ENABLE FEATURE
  add_theme_support('post-thumbnails');

  // CUSTOM POST TYPES

  // PARTNER
  register_post_type('partner', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Partner',
      'add_new_item' => 'Add New Partner',
      'edit_item' => 'Edit Partner',
      'all_items' => 'All Partner',
      'singular_name' => 'partner',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Client
  register_post_type('client', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Client',
      'add_new_item' => 'Add New client',
      'edit_item' => 'Edit client',
      'all_items' => 'All client',
      'singular_name' => 'client',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Webinar
  register_post_type('webinar', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Webinar',
      'add_new_item' => 'Add New webinar',
      'edit_item' => 'Edit webinar',
      'all_items' => 'All webinar',
      'singular_name' => 'webinar',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Career
  register_post_type('career', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Career',
      'add_new_item' => 'Add New career',
      'edit_item' => 'Edit career',
      'all_items' => 'All career',
      'singular_name' => 'career',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Team
  register_post_type('team', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Team',
      'add_new_item' => 'Add New team',
      'edit_item' => 'Edit team',
      'all_items' => 'All team',
      'singular_name' => 'team',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Press Release
  register_post_type('press-release', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Press Release',
      'add_new_item' => 'Add New press release',
      'edit_item' => 'Edit press release',
      'all_items' => 'All press releases',
      'singular_name' => 'press-release',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Event
  register_post_type('event', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Event',
      'add_new_item' => 'Add New event',
      'edit_item' => 'Edit event',
      'all_items' => 'All event',
      'singular_name' => 'event',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Whitepaper
  register_post_type('whitepaper', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Whitepaper',
      'add_new_item' => 'Add New whitepaper',
      'edit_item' => 'Edit whitepaper',
      'all_items' => 'All whitepaper',
      'singular_name' => 'whitepaper',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // Testimonials
  register_post_type('testimonial', array(
    'has_archive' => TRUE,
    'public' => TRUE,
    'labels' => array(
      'name' => 'Testimonial',
      'add_new_item' => 'Add New Testimonial',
      'edit_item' => 'Edit Testimonial',
      'all_items' => 'All Testimonial',
      'singular_name' => 'testimonial',
    ),
    'menu_icon' => 'dashicons-arrow-right-alt2',
    'show_in_menu' => TRUE,
    'taxonomies' => array('category'),
  ));

  // XXX
  // register_post_type('xxx', array(
  //   'has_archive' => TRUE,
  //   'public' => TRUE,
  //   'labels' => array(
  //     'name' => 'XXX',
  //     'add_new_item' => 'Add New xxx',
  //     'edit_item' => 'Edit xxx',
  //     'all_items' => 'All xxx',
  //     'singular_name' => 'xxx',
  //   ),
  //   'menu_icon' => 'dashicons-arrow-right-alt2',
  //   'show_in_menu' => TRUE,
  //   'taxonomies' => array('category'),
  // ));

  remove_post_type_support("partner", "editor");
  remove_post_type_support("client", "editor");
  remove_post_type_support("testimonial", "editor");
  remove_post_type_support("webinar", "editor");
  remove_post_type_support("whitepaper", "editor");
  remove_post_type_support("page", "editor");
}

// SET IMAGE SIZE ###################################################################################
add_action('after_setup_theme', 'set_image_size');
function set_image_size() {
  add_image_size('sq176', 176, 176, true);
  add_image_size('opengraph', 1200, 630, true);
  add_image_size('twittercard', 800, 420, true);
  add_image_size('desktop', 1920, 1080, true);
  add_image_size('tablet', 1336, 1024, true);
  add_image_size('phone', 600, 600, true);
  add_image_size('rect300x210', 300, 210, true);
  add_image_size('rect200x140', 200, 140, true);
  add_image_size('sq238x210', 238, 210, true);
  add_image_size('sq170x150', 170, 150, true);
}

// HELPER FUNCTIONS #################################################################################
function q_get_img_src($imgid, $imgsize = null) {
  return preg_replace(['/^.*src="/', '/" class=".*/'], '', wp_get_attachment_image($imgid, $imgsize));
}

// CUSTOM ENDPOINTS #################################################################################
add_action('rest_api_init', 'custom_endpoints');
function custom_endpoints() {
  // HOMEPAGE ENDPOINT
    register_rest_route('homepage/v1', 'content', [
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'homepage_content',
    ]);
  // WEBINAR ENDPOINT
    register_rest_route('webinar/v1', 'content', [
      'methods' => WP_REST_SERVER::READABLE,
      'callback' => 'webinar_content',
  ]);
  // WHITEPAPER ENDPOINT
  register_rest_route('whitepaper/v1', 'content', [
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'whitepaper_content',
  ]);
  // PARTNER ENDPOINT
  register_rest_route('partner/v1', 'content', [
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'partner_content',
  ]);
  // CLIENT ENDPOINT
  register_rest_route('client/v1', 'content', [
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'client_content',
  ]);

  // ! TEST
  register_rest_route('test/v1', 'content', [
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'test_content',
  ]);
}
  // ! TEST
  function test_content() {
    $data = [];
    $data['form'] = do_shortcode( '[contact-form-7 title="webinar form"]' );
    return $data;
  }

  
// HOMEPAGE ENDPOINT
// HOMEPAGE ENDPOINT
// HOMEPAGE ENDPOINT
function homepage_content() {
  $data = [];
  $testimonial_data = [];

  // loop through data
  $homepage = new WP_Query([
    'pagename' => 'homepage',
    'post_type' => 'page',
  ]);
  while($homepage->have_posts()) {
    $homepage->the_post();

    $data['meta'] = [
      'title' => get_field('homepage_metadata')['page_title'],
      'desc' => get_field('homepage_metadata')['page_description'],
      'opengraph_img' => q_get_img_src(get_field('homepage_metadata')['page_image'], 'opengraph'),
      'twitter_img' => q_get_img_src(get_field('homepage_metadata')['page_image'], 'twittercard'),
      'img_alt' => get_field('homepage_metadata')['page_image_alternative_text'],
      'twitter_handle' => get_field('homepage_metadata')['twitter_handle'],
    ];

    $data['s1'] = [
      'bg_img' => q_get_img_src(get_field('homepage_section_1')['background_image']),
      'heading' => get_field('homepage_section_1')['heading'],
      'caption' => get_field('homepage_section_1')['caption'],
      'img_src' => q_get_img_src(get_field('homepage_section_1')['image']),
      'img_alt' => get_field('homepage_section_1')['image_alt_text'],
      'logos' => get_field('homepage_section_1')['client_logo'],
    ];

    $data['s2'] = [
      'heading' => get_field('homepage_section_2')['heading'],
      'card1_heading' => get_field('homepage_section_2')['card1_heading'],
      'card1_caption' => get_field('homepage_section_2')['card1_caption'],
      'card2_heading' => get_field('homepage_section_2')['card2_heading'],
      'card2_caption' => get_field('homepage_section_2')['card2_caption'],
      'card3_heading' => get_field('homepage_section_2')['card3_heading'],
      'card3_caption' => get_field('homepage_section_2')['card3_caption'],
    ];

    $data['s3'] = [
      'heading' => get_field('homepage_section_3')['heading'],
      'caption' => get_field('homepage_section_3')['caption'],
      'a_text' => get_field('homepage_section_3')['button_text'],
      'a_href' => get_field('homepage_section_3')['button_link'],
      'a_title' => get_field('homepage_section_3')['link_description'],
      'a_target' => get_field('homepage_section_3')['open_link_in_new_tab'],
    ];

    $data['s4'] = [
      'heading' => get_field('homepage_section_4')['heading'],
      'caption' => get_field('homepage_section_4')['caption'],
      'logos' => get_field('homepage_section_4')['client_logo'],
      'a_text' => get_field('homepage_section_4')['button_text'],
      'a_href' => get_field('homepage_section_4')['button_link'],
      'a_title' => get_field('homepage_section_4')['link_description'],
      'a_target' => get_field('homepage_section_4')['open_link_in_new_tab'],
    ];

    $data['s5'] = [
      'card1_heading' => get_field('homepage_section_5')['card1_heading'],
      'card1_caption' => get_field('homepage_section_5')['card1_caption'],
      'card2_heading' => get_field('homepage_section_5')['card2_heading'],
      'card2_caption' => get_field('homepage_section_5')['card2_caption'],
      'card3_heading' => get_field('homepage_section_5')['card3_heading'],
      'card3_caption' => get_field('homepage_section_5')['card3_caption'],
    ];

    $data['s6'] = [
      'card1_heading' => get_field('homepage_section_6')['card1_heading'],
      'card1_caption' => get_field('homepage_section_6')['card1_caption'],
      'card2_heading' => get_field('homepage_section_6')['card2_heading'],
      'card2_caption' => get_field('homepage_section_6')['card2_caption'],
      'card3_heading' => get_field('homepage_section_6')['card3_heading'],
      'card3_caption' => get_field('homepage_section_6')['card3_caption'],
      'card4_heading' => get_field('homepage_section_6')['card4_heading'],
      'card4_caption' => get_field('homepage_section_6')['card4_caption'],
    ];

    $data['s7'] = [
      'heading' => get_field('homepage_section_7')['heading'],
    ];
  }

  $testimonial = new WP_Query([
    'post_type' => 'testimonial',
  ]);
  while($testimonial->have_posts()) {
    $testimonial->the_post();

    array_push($testimonial_data, [
      'entity' => get_the_title(),
      'img_src' => q_get_img_src(get_field('testimonial_image')),
      'img_alt' => get_field('testimonial_image_alt_text'),
      'rep_position' => get_field('testimonial_position'),
      'rep_name' => get_field('testimonial_name'),
      'testimonial' => get_field('testimonial_body'),
      'a_href' => get_field('testimonial_project_link'),
      'a_title' => get_field('testimonial_link_description'),
      'a_target' => get_field('testimonial_open_link_in_new_tab'),
    ]);
  }

  $data['testimonial'] = $testimonial_data;
  
  return $data;
}
// WEBINAR ENDPOINT
// WEBINAR ENDPOINT
// WEBINAR ENDPOINT
function webinar_content() {
  $today = date("Y-m-d H:i:s");
  $data = [];

  // loop through page data
  $webinar = new WP_Query([
    'pagename' => 'webinar',
    'post_type' => 'page',
  ]);
  while($webinar->have_posts()) {
    $webinar->the_post();

    $data['today'] = $today;
    $data['current_datetime'] = date("F j, Y, G:i");
    $data['current_datetime_unix'] = strtotime(date('Y-m-d g:ia'));

    $data['meta'] = [
      'title' => get_field('webinar_metadata')['page_title'],
      'desc' => get_field('webinar_metadata')['page_description'],
      'opengraph_img' => q_get_img_src(get_field('webinar_metadata')['page_image'], 'opengraph'),
      'twitter_img' => q_get_img_src(get_field('webinar_metadata')['page_image'], 'twittercard'),
      'img_alt' => get_field('webinar_metadata')['page_image_alternative_text'],
      'twitter_handle' => get_field('webinar_metadata')['twitter_handle'],
    ];

    $data['s1'] = [
      'heading1' => get_field('webinar_section_1')['heading1'],
      'heading2' => get_field('webinar_section_1')['heading2'],
      // 'time' => current_time('Y-m-d : ga'),
    ];
    
    $data['upcoming'] = [];
    $data['previous'] = [];
  }
  
  // loop through UPCOMING webinars
  $upcoming = new WP_Query([
    'post_type' => 'webinar',
    'meta_key' => 'webinar_post_content_date_time',
    'orderby' => 'date',
    'order' => 'desc',
    'posts_per_page' => 6,
    'meta_query' => [[
      'key' => 'webinar_post_content_date_time',
      'compare' => '>',
      'value' => $today,
      'type' => 'DATETIME'
    ]]
  ]);
  while($upcoming->have_posts()) {
    $upcoming->the_post();
    array_push($data['upcoming'], [
      'title' => get_the_title(),
      'content' => get_field('webinar_post_content')['content'],
      // 'webinar_date_time' => date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])),
      'month' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[0],
      'date' => str_replace(',','',explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[1]),
      'year' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[2],
      'start_time' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[3],
      'end_time' => get_field('webinar_post_content')['end_time'],
      'label' => get_field('webinar_post_content')['label'],
    ]);
  }

  // loop through past webinars
  $previous = new WP_Query([
    'post_type' => 'webinar',
    'meta_key' => 'webinar_post_content_date_time',
    'orderby' => 'date',
    'order' => 'desc',
    'posts_per_page' => 6,
    'meta_query' => [[
      'key' => 'webinar_post_content_date_time',
      'compare' => '<',
      'value' => $today,
      'type' => 'DATETIME'
    ]]
  ]);
  while($previous->have_posts()) {
    $previous->the_post();
    array_push($data['previous'], [
      'title' => get_the_title(),
      'content' => get_field('webinar_post_content')['content'],
      'img_src' => q_get_img_src(get_field('webinar_post_content')['page_image'], 'sq176'),
      'img_alt' => get_field('webinar_post_content')['image_alt_text'],
      // 'webinar_date_time' => date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])),
      'month' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[0],
      'date' => str_replace(',','',explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[1]),
      'year' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[2],
      'start_time' => explode(' ', date('F j, Y H:i', strtotime(get_field('webinar_post_content')['date_time'])))[3],
      'end_time' => get_field('webinar_post_content')['end_time'],
      'label' => get_field('webinar_post_content')['label'],
    ]);
  }

  return $data;
}

// WHITEPAPER ENDPOINT
// WHITEPAPER ENDPOINT
// WHITEPAPER ENDPOINT
function whitepaper_content() {
  $data = [];
  $whitepaper_data = [];

  // loop through page data
  $page_whitepaper = new WP_Query([
    'pagename' => 'whitepaper',
    'post_type' => 'page',
  ]);

  while($page_whitepaper->have_posts()) {
    $page_whitepaper->the_post();
    $data['meta'] = [
      'title' => get_field('whitepaper_metadata')['page_title'],
      'desc' => get_field('whitepaper_metadata')['page_description'],
      'opengraph_img' => q_get_img_src(get_field('whitepaper_metadata')['page_image'], 'opengraph'),
      'twitter_img' => q_get_img_src(get_field('whitepaper_metadata')['page_image'], 'twittercard'),
      'img_alt' => get_field('whitepaper_metadata')['page_image_alternative_text'],
      'twitter_handle' => get_field('whitepaper_metadata')['twitter_handle'],
    ];

    $data['s1'] = [
      'heading1' => get_field('whitepaper_section_1')['heading1'],
      'heading2' => get_field('whitepaper_section_1')['heading2'],
    ];
  }

  // loop through whitepaper post type
  $post_whitepaper = new WP_Query([
    'post_type' => 'whitepaper',
  ]);
  
  while($post_whitepaper->have_posts()) {
    $post_whitepaper->the_post();
    
    array_push($whitepaper_data, [
      'img_src_desktop_tablet' => q_get_img_src(get_field('whitepaper_post_content')['page_image'], 'rect300x210'),
      'img_src_phone' => q_get_img_src(get_field('whitepaper_post_content')['page_image'], 'rect200x140'),
      'img_alt' => get_field('whitepaper_post_content')['image_alt_text'],
      'label' => get_field('whitepaper_post_content')['label'],
      'title' => get_the_title(),
      'content' => get_field('whitepaper_post_content')['content'],
    ]);

    $data['whitepapers'] = $whitepaper_data;
  }

  return $data;
}

// PARTNER ENDPOINT
// PARTNER ENDPOINT
// PARTNER ENDPOINT
function partner_content() {
  $data = [];
  $partners_data = [];

  // loop through page data
  $page_partner = new WP_Query([
    'pagename' => 'partner',
    'post_type' => 'page',
  ]);

  while($page_partner->have_posts()) {
    $page_partner->the_post();
    $data['meta'] = [
      'title' => get_field('partner_metadata')['page_title'],
      'desc' => get_field('partner_metadata')['page_description'],
      'opengraph_img' => q_get_img_src(get_field('partner_metadata')['page_image'], 'opengraph'),
      'twitter_img' => q_get_img_src(get_field('partner_metadata')['page_image'], 'twittercard'),
      'img_alt' => get_field('partner_metadata')['page_image_alternative_text'],
      'twitter_handle' => get_field('partner_metadata')['twitter_handle'],
    ];

    $data['s1'] = [
      'heading1' => get_field('partner_section_1')['heading1'],
      'heading2' => get_field('partner_section_1')['heading2'],
    ];
  }

  // loop through whitepaper post type
  $post_partners = new WP_Query([
    'post_type' => 'partner',
  ]);
  
  while($post_partners->have_posts()) {
    $post_partners->the_post();
    
    array_push($partners_data, [
      'img_src_desktop_tablet' => q_get_img_src(get_field('partner_post_content')['partner_image'], 'sq238x210'),
      'img_src_phone' => q_get_img_src(get_field('partner_post_content')['partner_image'], 'sq170x150'),
      'img_alt' => get_field('partner_post_content')['image_alt_text'],
      'title' => get_the_title(),
      'label' => get_field('partner_post_content')['label'],
      'content' => get_field('partner_post_content')['content'],
    ]);

    $data['partners'] = $partners_data;
  }

  return $data;
}

// CLIENT ENDPOINT
// CLIENT ENDPOINT
// CLIENT ENDPOINT
function client_content() {
  $data = [];
  $client_data = [];

  // loop through page data
  $page_client = new WP_Query([
    'pagename' => 'client',
    'post_type' => 'page',
  ]);

  while($page_client->have_posts()) {
    $page_client->the_post();
    $data['meta'] = [
      'title' => get_field('client_metadata')['page_title'],
      'desc' => get_field('client_metadata')['page_description'],
      'opengraph_img' => q_get_img_src(get_field('client_metadata')['page_image'], 'opengraph'),
      'twitter_img' => q_get_img_src(get_field('client_metadata')['page_image'], 'twittercard'),
      'img_alt' => get_field('client_metadata')['page_image_alternative_text'],
      'twitter_handle' => get_field('client_metadata')['twitter_handle'],
    ];

    $data['s1'] = [
      'heading1' => get_field('client_section_1')['heading1'],
      'heading2' => get_field('client_section_1')['heading2'],
    ];
  }

  // loop through whitepaper post type
  $post_client = new WP_Query([
    'post_type' => 'client',
  ]);
  
  while($post_client->have_posts()) {
    $post_client->the_post();
    
    array_push($client_data, [
      'img_src_desktop_tablet' => q_get_img_src(get_field('client_post_content')['partner_image'], 'sq238x210'),
      'img_src_phone' => q_get_img_src(get_field('client_post_content')['partner_image'], 'sq170x150'),
      'img_alt' => get_field('client_post_content')['image_alt_text'],
      'title' => get_the_title(),
      'label' => get_field('client_post_content')['label'],
      'content' => get_field('client_post_content')['content'],
    ]);

    $data['clients'] = $client_data;
  }

  return $data;
}